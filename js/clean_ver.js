$(document).ready(function() {

	

	window.onscroll = function() {

		onScollFunction()

	};



	var header = $('.main-nav');

	var rotator = $('.image-rotator');

	var horizontalButton = $('.horizontal-btn');


	// FOR SECTION 1

	var s1vid = $('#s1vid');

	var s1p1 = $('#s1p1');

	var s1p2 = $('#s1p2');

	var s1p3 = $('#s1p3');

	var cta_btn1 = $('#cta-btn1');

	var s1_text = $('.s1-text');


	// FOR SECTION 2

	var s2vid = $('#s2vid');

	var s2p1 = $('#s2p1');

	var s2p2 = $('#s2p2');

	var s2p3 = $('#s2p3');

	var cta_btn2 = $('#cta-btn2');

	var s2_text = $('.s2-text');



	// FOR SECTION 3

	var s3vid = $('#s3vid');

	var s3p1 = $('#s3p1');

	var s3p2 = $('#s3p2');

	var s3p3 = $('#s3p3');

	var cta_btn3 = $('#cta-btn3');

	var s3_text = $('.s3-text');



	var stickyHeader = header.offset().top;

	var stickyHorizontalButton = horizontalButton.offset().top;




	// START OF onScollFunction()
	function onScollFunction() {



	  // For sticky header

	  if (window.pageYOffset > stickyHeader) {

	    header.addClass("sticky");

	    rotator.css('margin-top', '50px');

	  } else {

	    header.removeClass("sticky");

	    rotator.css('margin-top', '0px');

	  }



	  // For sticky horizontal button

	  if (window.pageYOffset > 60 || window.pageYOffset > stickyHorizontalButton) {

	  	horizontalButton.css({'position': 'fixed', 'top': -30});

	  } else {

		horizontalButton.css({'position': 'absolute', 'top': 0});

	  }


	}
	// END OF onScollFunction()

	


	//START OF VISIBILITY FUNC
	$.fn.isInViewport = function() {
	    var elementTop = $(this).offset().top;
	    var elementBottom = elementTop + $(this).outerHeight();

	    var viewportTop = $(window).scrollTop();
	    var viewportBottom = viewportTop + $(window).height();

	    return elementBottom > viewportTop && elementTop < viewportBottom;
	};
	//END OF VISIBILITY FUNC


	//START OF CUSTOM SCROLL

	$(window).scroll(function(){

		//FOR s1vid
		if (window.pageYOffset > 540 || window.pageYOffset > stickyHorizontalButton) {

			s1vid.css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'transform' : 'translate(-50%, 0)'});

		} else {

		s1vid.css({'position': 'relative' ,'left':0, 'transform' : 'translate(0, 0)'});

		}


		//FOR .s1-text #s1p1

		if (window.pageYOffset >= 600) {

			s1_text.css({'text-align' : 'right' , 'margin-top' : '200px'});
			s1p1.css({'text-align' : 'right' , 'opacity' : 1 ,'animation' : 'fade-in', 'font-size': '32px','animation': 'fadeInUp linear 1s', 'animation-delay': '1s','-webkit-animation': 'fadeInUp linear 1s', '-moz-animation': 'fadeInUp linear 1s','-o-animation': 'fadeInUp linear 1s','-ms-animation': 'fadeInUp linear 1s'});

		} else {

			s1p1.css({'opacity' : 0 ,'animation' : 'fade-out linear 1s'});

		}

		if(s1_text.isInViewport()){
			console.log('nakita');
		}

		//IF s1vid AND ALL s1-text is Displayed
		if(s1_text.isInViewport() && window.pageYOffset >= 1333){
			//s1vid.css({'position': 'static' ,'left':0, 'transform' : 'translate(0, 0)'});
			s1vid.css({'position': 'relative' , 'left':0, 'bottom': -700 , 'animation' : 'fade-out linear 1s', 'transform' : 'translate(0, 0)'});

		}
		
		if( window.pageYOffset >= 1334){
		
			s1vid.css({'position': 'relative' , 'left':0, 'bottom': -700 , 'animation' : 'fade-out linear 1s', 'transform' : 'translate(0, 0)'});

		}

		// if (window.pageYOffset >= 540) {
		// 	s1_text.css({'text-align' : 'right' , 'margin-top' : '200px'});
		// 	s1vid.css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'transform' : 'translate(-50%, 0)'});

		// } else if(window.pageYOffset >= 800){

		// 	s1p1.css({'position' : 'relative' , 'text-align' : 'right' , 'opacity' : 1 ,'animation' : 'fade-in', 'font-size': '32px','animation': 'fadeInUp linear 1s', 'animation-delay': '1s','-webkit-animation': 'fadeInUp linear 1s', '-moz-animation': 'fadeInUp linear 1s','-o-animation': 'fadeInUp linear 1s','-ms-animation': 'fadeInUp linear 1s'});
		// 	s1p2.css({'position' : 'relative' ,'text-align' : 'right' , 'opacity' : 1 ,'animation' : 'fade-in', 'font-size': '24px','animation': 'fadeInUp linear 1s', 'animation-delay': '7s','-webkit-animation': 'fadeInUp linear 1s', '-moz-animation': 'fadeInUp linear 1s','-o-animation': 'fadeInUp linear 1s','-ms-animation': 'fadeInUp linear 1s'});
		// 	s1p3.css({'position' : 'relative' ,'text-align' : 'right' , 'opacity' : 1 ,'animation' : 'fade-in', 'font-size': '24px','animation': 'fadeInUp linear 1s', 'animation-delay': '12s','-webkit-animation': 'fadeInUp linear 1s', '-moz-animation': 'fadeInUp linear 1s','-o-animation': 'fadeInUp linear 1s','-ms-animation': 'fadeInUp linear 1s'});
		// 	cta_btn1.css({'position' : 'relative' ,'text-align' : 'right' , 'opacity' : 1 ,'animation' : 'fade-in', 'animation': 'fadeInUp linear 1s', 'animation-delay': '18s','-webkit-animation': 'fadeInUp linear 1s', '-moz-animation': 'fadeInUp linear 1s','-o-animation': 'fadeInUp linear 1s','-ms-animation': 'fadeInUp linear 1s'});

		// } else if(window.pageYOffset >= 1300){
	  	
		//   	s1vid.css({'position': 'static' , 'left':0, 'bottom': -700 , 'animation' : 'fade-out linear 1s', 'transform' : 'translate(0, 0)'});
			
		// } else {

		// 	s1vid.css({'position': 'relative' , 'left':0, 'transform' : 'translate(0, 0)'});
		// 	s1p1.css({'position': 'relative' , 'opacity' : 0 ,'animation' : 'fade-out linear 1s'});
		// 	s1p2.css({'position': 'relative' , 'opacity' : 0 ,'animation' : 'fade-out linear 1s'});
		// 	s1p3.css({'position': 'relative' , 'opacity' : 0 ,'animation' : 'fade-out linear 1s'});
		// 	cta_btn1.css({'position': 'relative' , 'opacity' : 0 ,'animation' : 'fade-out linear 1s'});
		// }

	});
	


	//END OF CUSTOM SCROLL


	//Check if scrolling stops

	// const onScrollStop = callback => {
	//   let isScrolling;
	//   let ss = 0;
	//   window.addEventListener(
	//     'scroll',
	//     e => {
	//       clearTimeout(isScrolling);
	//       isScrolling = setTimeout(() => {
	//         callback();
	//       }, 150);
	//     },
	//     false
	//   );
	// };
	// onScrollStop(() => {

	//   console.log('The user has stopped scrolling');

	// });
	



	//Get current scroll point
	window.addEventListener("scroll", function(){  console.log(this.scrollY)  })

});