document.cookie.split(";")
  .forEach(function(c) { 
  	document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

var header = $('.main-nav');

var rotator = $('.image-rotator');

var horizontalButton = $('.horizontal-btn');






if(header.length > 0){

	var stickyHeader = header.offset().top;

}

if(horizontalButton.length > 0){

	var stickyHorizontalButton = horizontalButton.offset().top;
}


var carouselLength = $('.carousel-item').length - 1;
var clipcount = 1;
var currclip = "";


$(document).ready(function() {

	window.scrollTo(0, 0);
	$('#hcard1').css({'left':'-350px'});
	$('#hcard2').css({'top':'350px',"left":"-350px"});
	$('#hcard3').css({'top':'100px',"left":"-350px"});
	$('#hbtn1').on('click', function() {
		$('#hcard2').animate({"left":"-350px"}, "slow").addClass('hidden');
		$('#hcard3').animate({"left":"-350px"}, "slow").addClass('hidden');

		if($('#hcard1').hasClass('hidden')){
			$('#hcard1').animate({"left":"40px"}, "slow").removeClass('hidden');
		}
		else {
			 $('#hcard1').animate({"left":"-350px"}, "slow").addClass('hidden');
		}	

	});

	$('#hbtn2').on('click', function() {
		$('.hcard2').css({'top':'350px','left':'-350px'});
		$('#hcard1').animate({"left":"-350px"}, "slow").addClass('hidden');
		$('#hcard3').animate({"left":"-350px"}, "slow").addClass('hidden');

		if($('#hcard2').hasClass('hidden')){
			$('#hcard2').animate({'top':'350px',"left":"40px"}, "slow").removeClass('hidden');
		}
		else {
			 $('#hcard2').animate({'top':'350px',"left":"-350px"}, "slow").addClass('hidden');
		}	

	});


	$('#hbtn3').on('click', function() {
		$('#hcard1').animate({'top':'100px',"left":"-350px"}, "slow").addClass('hidden');
		$('#hcard2').animate({'top':'100px',"left":"-350px"}, "slow").addClass('hidden');

		if($('#hcard3').hasClass('hidden')){
			$('#hcard3').animate({'top':'100px',"left":"40px"}, "slow").removeClass('hidden');
		}
		else {
			 $('#hcard3').animate({'top':'100px',"left":"-350px"}, "slow").addClass('hidden');
		}	

	});



	// If there is more than one item
	if (carouselLength) {
	    $('.carousel-control-next').removeClass('d-none');
	}

	$('#carouselExampleIndicators').carousel({
	    interval: false,
	    wrap: false
	}).on('slide.bs.carousel', function (e) {
	    // First one
	    if (e.to == 0) {
	        $('.carousel-control-prev').addClass('d-none');
	        $('.carousel-control-next').removeClass('d-none');
	    } // Last one
	    else if (e.to == carouselLength) {
	        $('.carousel-control-prev').removeClass('d-none');
	        $('.carousel-control-next').addClass('d-none');
	    } // The rest
	    else {
	        $('.carousel-control-prev').removeClass('d-none');
	        $('.carousel-control-next').removeClass('d-none');
	    }
	});



	window.onscroll = function() {

		onScollFunction()

	};

	// START OF onScollFunction()
	function onScollFunction() {

		// For sticky header

		if (window.pageYOffset > stickyHeader) {

			header.addClass("sticky");

			rotator.css('margin-top', '50px');

		} else {

			header.removeClass("sticky");

			rotator.css('margin-top', '0px');

		}



		// For sticky horizontal button

		if (window.pageYOffset > 60 || window.pageYOffset > stickyHorizontalButton) {

			horizontalButton.css({'position': 'fixed', 'top': '-5%'});
			$('.hcard').css({'position': 'fixed', 'top': '25%'});

		} else {

			horizontalButton.css({'position': 'absolute', 'top': '50px'});
			$('.hcard').css({'position': 'absolute', 'top': '200px'});
			

		}


		// For sticky horizontal cards
		// if (window.pageYOffset > 10) {

		
		// 	$('.hcard').css({'position': 'fixed', 'top': '20%'});

		// }
		// else {
		// 	$('.hcard').css({'position': 'absolute', 'top': '250px'});
		// }


		// For #carouselExampleIndicators  /////////////////////////////////////////////////////
		if(window.pageYOffset >= 300) {

			

			$(window).bind('mousewheel DOMMouseScroll', function(event){

				if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
					// scroll up
					
					$('video').on('play', function(){
						$(this).each(function() {

						   if( $(this).attr('id') == "s1vid"){
								$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 170});
								$('body').css({'overflow' : 'scroll'});
							}
							else if( $(this).attr('id') == "s16vid"){
								$('#carouselExampleIndicators').removeAttr('style');
								$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 0});
								$('body').css({'overflow' : 'scroll'});
							}
							else if($(this).attr('id') != "s1vid" || $(this).attr('id') != "s16vid") {
								$("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'top' : 150, 'transform' : 'translate(-50%, 0)'});
								$('body').css({'overflow' : 'hidden'});
							}
							else if(currclip != "s16vid"){
								$('.loadmore-btn').trigger("click");
							}

							currclip = $(this).attr('id');
							clipcount += 1;
							// console.log('curr vid: '+$(this).attr('id'));
							// console.log('currclip: '+currclip);
						});
					});
					
					$('.carousel-control-prev').trigger("click");

				}
				else {
					// scroll down

					$('video').on('play', function(){
						$(this).each(function() {
							if($(this).attr('id') == "s1vid"){
								$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 170});
								$('body').css({'overflow' : 'scroll'});
							}
							else if( $(this).attr('id') == "s16vid"){
							
								$('#carouselExampleIndicators').removeAttr('style');
								$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'relative' ,'left':0, 'top' : 300});
								$('body').css({'overflow' : 'scroll'});
							}
							else if($(this).attr('id') != "s1vid" && window.pageYOffset >= 300 || $(this).attr('id') != "s16vid" && window.pageYOffset >= 400) {
								$("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'top' : 150, 'transform' : 'translate(-50%, 0)'});
						
								$('body').css({'overflow' : 'hidden'});
							}

							currclip = $(this).attr('id');
							clipcount -= 1;
							// console.log('curr vid: '+$(this).attr('id'));
							// console.log('currclip: '+currclip);
						});
					});
					
					$('.carousel-control-next').trigger("click");
				}
			});

		}
		else if (currclip == "s16vid" || currclip == "") {
	
			$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 0});

		}
		else if(currclip != "s16vid"){
			$('.loadmore-btn').trigger("click");
		}
		// else if(currclip != "s16vid"){
		// 	$(".loadmore-btn").css({'margin-top':'500px'});
		// }
		else if (window.pageYOffset < 300) {
			$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 0});
			$('body').css({'overflow' : 'scroll'});
		}

		

	}

	$('.loadmore').slideToggle();
	$('.loadmore-btn').on('click', function(){
		$('.loadmore').slideToggle();
	});

	


	
	//Get current scroll point
	window.addEventListener("scroll", function(){  console.log(this.scrollY)  });



});