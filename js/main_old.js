
var header = $('.main-nav');

var rotator = $('.image-rotator');

var horizontalButton = $('.horizontal-btn');


if(header.length > 0){

	var stickyHeader = header.offset().top;

}

if(horizontalButton.length > 0){

	var stickyHorizontalButton = horizontalButton.offset().top;
}


var carouselLength = $('.carousel-item').length - 1;
var clipcount = 1;
var currclip = "";


$(document).ready(function() {

	window.scrollTo(0, 0);

	// If there is more than one item
	if (carouselLength) {
	    $('.carousel-control-next').removeClass('d-none');
	}

	$('#carouselExampleIndicators').carousel({
	    interval: false,
	    wrap: false
	}).on('slide.bs.carousel', function (e) {
	    // First one
	    if (e.to == 0) {
	        $('.carousel-control-prev').addClass('d-none');
	        $('.carousel-control-next').removeClass('d-none');
	    } // Last one
	    else if (e.to == carouselLength) {
	        $('.carousel-control-prev').removeClass('d-none');
	        $('.carousel-control-next').addClass('d-none');
	    } // The rest
	    else {
	        $('.carousel-control-prev').removeClass('d-none');
	        $('.carousel-control-next').removeClass('d-none');
	    }
	});



	window.onscroll = function() {

		onScollFunction()

	};

	// START OF onScollFunction()
	function onScollFunction() {

	  // For sticky header

	  if (window.pageYOffset > stickyHeader) {

	    header.addClass("sticky");

	    rotator.css('margin-top', '50px');

	  } else {

	    header.removeClass("sticky");

	    rotator.css('margin-top', '0px');

	  }



	  // For sticky horizontal button

	  if (window.pageYOffset > 60 || window.pageYOffset > stickyHorizontalButton) {

	  	horizontalButton.css({'position': 'fixed', 'top': '-5%'});

	  } else {

		horizontalButton.css({'position': 'absolute', 'top': '50px'});

	  }





	  // For s1vid	
	  	if (window.pageYOffset > 700 || window.pageYOffset > stickyHorizontalButton) {
	  		$("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'top' : 150, 'transform' : 'translate(-50%, 0)'});
				  	

	  		/////////////MEDIA QUERY 
			//@media 576px

			// var mq1 = window.matchMedia( "(max-width: 575px)" );

			// if (mq1.matches) {

			// 	// window width is at less than 576px
			//     if (window.pageYOffset > 315 || window.pageYOffset > stickyHorizontalButton) {

			// 	  	$("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'top' : 170, 'transform' : 'translate(-50%, 0)'});
				  	
			// 	  	var container = document.querySelector("#carouselExampleIndicators");

			// 		  container.addEventListener("touchstart", startTouch, false);
			// 		  container.addEventListener("touchmove", moveTouch, false);

			// 		  // Swipe Up / Down / Left / Right
			// 		  var initialX = null;
			// 		  var initialY = null;

			// 		  function startTouch(e) {
			// 		    initialX = e.touches[0].clientX;
			// 		    initialY = e.touches[0].clientY;
			// 		  };

			// 		  function moveTouch(e) {
			// 		    if (initialX === null) {
			// 		      return;
			// 		    }

			// 		    if (initialY === null) {
			// 		      return;
			// 		    }

			// 		    var currentX = e.touches[0].clientX;
			// 		    var currentY = e.touches[0].clientY;

			// 		    var diffX = initialX - currentX;
			// 		    var diffY = initialY - currentY;

			// 		    if (Math.abs(diffX) > Math.abs(diffY)) {
			// 		    	alert('pang mobile');
			// 		      // sliding horizontally
			// 		      if (diffX > 0) {
			// 		        // swiped left
			// 		        console.log("swiped left");
			// 		      } else {
			// 		        // swiped right
			// 		        console.log("swiped right");
			// 		      }  
			// 		    } else {
			// 		      // sliding vertically
			// 		      if (diffY > 0) {
			// 		        // swiped up
			// 		        console.log("swiped up");
			// 		        console.log(currentY);

			// 		        $('.carousel-control-next').trigger("click");
			// 		      }
			// 		      else if(this.scrollY >= 100) {
					      
			// 		      	$('.carousel-control-next').trigger("click");
			// 		      }
			// 		      else if(this.scrollY <= 100) {
			// 		      	$('.carousel-control-prev').trigger("click");
			// 		      }
			// 		      else {
			// 		        // swiped down
			// 		        console.log("swiped down");
			// 		        $('.carousel-control-prev').trigger("click");
			// 		      }  
			// 		    }

			// 		    initialX = null;
			// 		    initialY = null;

			// 		    e.preventDefault();
			// 		  };
  
				  	
			// 	}

			// }
			// else {

			//     // window width is greater than 576px
			//     //$("#carouselExampleIndicators").css({'width': '95%', 'margin': '20px auto 50px aut' , 'left' : '50%' , 'top' : 150, 'transform' : 'translate(-50%, 0)'});
			//     $("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50%' , 'top' : 150, 'transform' : 'translate(-50%, 0)'});

			// }

			if(window.pageYOffset >= 500){
				

				var myDiv = document.querySelector('.carousel-item');

				$(window).bind('mousewheel DOMMouseScroll', function(event){

					if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
						// scroll up
						$('.carousel-control-prev').trigger("click");
						clipcount -= 1;
						// console.log('prev count: '+clipcount);
						
						$('video').on('play', function (e) {

							console.log('vid_id: '+$(this).attr('id'));
						 //    $("#carouselExampleIndicators").carousel('pause');

							// $('video').on('stop pause ended', function (e) {
								
							//     $("#carouselExampleIndicators").carousel();
							// });

							$(this).each(function() {

							   if($(this).attr('id') != "s1vid"){
							   	
									$('body').css({'overflow' : 'hidden'});
								}
								else if( $(this).attr('id') == "s1vid"){
									$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 0});
									$('body').css({'overflow' : 'scroll'});
								}
								else if($(this).attr('id') != "s1vid" || $(this).attr('id') != "s16vid") {
									$('body').css({'overflow' : 'hidden'});
								}

							});
							
						});

					}
					else {
						// scroll down
						$('.carousel-control-next').trigger("click");

						clipcount += 1;
						// console.log('next count: '+clipcount);
						

						$('video').on('play', function (e) {

							console.log('vid_id: '+$(this).attr('id'));
						 //    $("#carouselExampleIndicators").carousel('pause');

							// $('video').on('stop pause ended', function (e) {
								
							//     $("#carouselExampleIndicators").carousel();
							// });

							$(this).each(function() {

							   if($(this).attr('id') == "s1vid"){
							   	
									$('body').css({'overflow' : 'scroll'});
								}
								else if( $(this).attr('id') == "s16vid"){
								
									$('body').css({'overflow' : 'scroll'});
								}
								else if($(this).attr('id') != "s1vid" || $(this).attr('id') != "s16vid") {
									$('body').css({'overflow' : 'hidden'});
								}

							});
							
						});
						
					}

					// if(clipcount<0){
					// 	$('body').css({'overflow' : 'scroll'});
					// }
					// else{
					// 	$('body').css({'overflow' : 'hidden'});
						
					// }

				});

				

			}


			
			// if (window.pageYOffset > 8700){

			// 	$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'static' ,'left':0, 'top' : 0});

			// }

		}
		else if(window.pageYOffset < 300){
			//window.scrollTo(0, 0);
			$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'relative' ,'left':0, 'top' : '-30px', 'width' : '100%'});

		}

		
		

		// if(clipcount < window.pageYOffset && window.pageYOffset <400){
					
		// 	$('body').css({'overflow-y':'scroll'});
		// 	$("#carouselExampleIndicators").css({'transform' : 'translate(0, 0)' , 'position': 'relative' ,'left':0, 'top' : '50px', 'width' : '100%'});
		
		// } else {
			
		// 	$('body').css({'overflow':'hidden'});
		// 	$("#carouselExampleIndicators").css({'position': 'fixed', 'bottom': 50 , 'left' : '50s%' , 'top' : 120, 'transform' : 'translate(-50%, 0)'});
		// }

		
	}

	
	

	
	


	//PAUSE CAROUSEL IF VIDEO ON PLAY
	$('video').on('play', function (e) {

	    $("#carouselExampleIndicators").carousel('pause');

		$('video').on('stop pause ended', function (e) {
			
		    $("#carouselExampleIndicators").carousel();
		})
	});

	//Get current scroll point
	window.addEventListener("scroll", function(){  console.log(this.scrollY)  });





});